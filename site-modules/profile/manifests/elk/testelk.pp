class profile::elk::testelk {

 include java
 include elastic_stack::repo

 $kibana_srv = lookup('profile::elk::kibana_srv')
 $elastic_srv = lookup('profile::elk::elastic_srv')

  class { 'elasticsearch': 
   package_ensure => latest,
}
  elasticsearch::instance { 'es-01': 
   config => {
       'network.host'   => '${elastic_srv}',
       'http.port'      => '9200',
       'discovery.type' => 'single-node',
   }
  }
  es_instance_conn_validator { 'es-01' :
   port => '9200',
 }

  class { 'logstash': 
   package_ensure => latest,
}
  logstash::configfile { 'logstash-syslog.conf':
    source => 'puppet:///modules/profile/logstash.conf',
}

  class { 'kibana': 
    package_ensure => latest,
    config => {
        'server.port'       => '5601',
        'server.host'       => '${kibana_srv}',
        'elasticsearch.url' => ["${elastic_srv}:9200"],
  }
 }

  class { 'nginx': }
   nginx::resource::server{ "${kibana_srv}":
   listen_port => 80,
   proxy       => 'http://localhost:5601',
 }
}
