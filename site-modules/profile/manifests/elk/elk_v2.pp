class profile::elk::elk_v2 {

 class { 'java': }

 class { 'elasticsearch':  
  ensure       => 'present',
  status       => 'enabled',
  manage_repo  => true,
  repo_stage   => false,
}
  elasticsearch::instance { 'es-01':
   config         => {
   'network.host' => '127.0.0.1',
   'http.port'    => '9200',
   'service.name' => 'elasticsearch-service',
   jvm_options    => [
    '-Xms1024m',
    '-Xmx1024m'
   ]
  }
 }

  service { "elasticsearch-service":
    name   => 'elasticsearch-service',
    ensure => 'running',
    status => 'enabled',
  }
}
