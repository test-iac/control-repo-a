#
# profile::elk::elk
#

class profile::elk::elk {

  include java
  include elastic_stack::repo

  class { 'elasticsearch':
  ensure      => 'present',
  jvm_options => [
    '-Xms256m',
    '-Xms256m'
    ]
    }
  elasticsearch::instance { 'es-01':
      config        => {
        'network.host'   => 'localhost',
        'http.port'      => '9200',
        'discovery.type' => 'single-node',
    }
    }
  class { 'logstash':
  ensure      => 'present',
  jvm_options => [
    '-Xms256m',
    '-Xms256m'
    ]
  }
  logstash::configfile { 'input.conf':
    content => 'input { beats { 
       port => 5044
       type => syslog 
     } 
   }'
  }
  logstash::configfile { 'filter.conf':
    content => 'filter {
   if [type] == "syslog" {
   grok {
     match => { "message" => "%{SYSLOGTIMESTAMP:syslog_timestamp} 
               %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?:
               %{GREEDYDATA:syslog_message}"
              }
     add_field => [ "received_at", "%{@timestamp}" ]
     add_field => [ "received_from", "%{host}" ]
  }
  date {
    match => [ "syslog_timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
    }
   }
  }',
  }
  logstash::configfile { 'output.conf':
    content => 'output { elasticsearch {
      hosts => ["localhost:9200"] 
    }    
   }',
  }

  class { 'kibana':
    ensure => 'latest',
    config => {
        'server.port'       => '5601',
        'server.host'       => 'localhost',
        'elasticsearch.url' => ['http://localhost:9200'],
  }
  }
  }
