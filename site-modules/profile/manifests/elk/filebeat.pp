#
# profile::elk::filebeat
#

class profile::elk::filebeat {

 $logstash_srv = lookup('profile::elk::logstash_srv')

  class { 'filebeat':
  package_ensure => latest,
  manage_repo    => true,
  outputs        => {
      'logstash' => {
      'hosts'    => [ "${logstash_srv}:5044" ],
    },
  },
  }

filebeat::input { 'syslogs':
    paths    => [
      '/var/log/syslog',
    ],
    doc_type => 'syslog-beat',
  }
}
