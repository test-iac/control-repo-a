node default {
  notify { "Oops Default! I'm ${facts['hostname']}": }
}

node 'manager.node.consul' {
  include ::role::manager_server
}

node 'dir.node.consul' {
  include ::role::directory_server
}

node 'mon.node.consul' {
  include ::role::monitoring_server
}

node /win1/ {
  include ::role::domain_joined_server
}

node 'lin1.node.consul' {
  include ::role::linux_server
}

node 'elk.node.consul' {
  include ::role::elk_server
}
